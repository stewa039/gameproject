
{
    "name": "rm_grass_parent",
    "id": "bcc16985-463c-4dff-9531-6a01dc18968e",
    "creationCodeFile": "RoomCreationCode.gml",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [

    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "be8645f9-8da2-42f7-a952-75ce2cc6f2a4",
            "depth": 0,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [

            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRTileLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Tiles",
            "id": "92fe8df9-28da-4c1f-bcf0-c7f3efdce502",
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRTileLayer",
            "prev_tileheight": 16,
            "prev_tilewidth": 16,
            "mvc": "1.0",
            "tiles": {
                "SerialiseData": null,
                "SerialiseHeight": 15,
                "SerialiseWidth": 20,
                "TileSerialiseData": [
                    17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,
                    17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,
                    17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,
                    17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,
                    17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,
                    17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,
                    17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,
                    17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,
                    17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,
                    17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,
                    17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,
                    17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,
                    17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,
                    17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,
                    17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17
                ]
            },
            "tilesetId": "05362a3e-12dc-44b1-ab94-3f9d8d8264de",
            "userdefined_depth": false,
            "visible": true,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "89801bfc-77f2-436a-b37b-6965b3d1d57f",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "6a2b70bd-78fc-49c3-b1d0-c395bce69520",
        "Height": 240,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": true,
        "mvc": "1.0",
        "Width": 320
    },
    "mvc": "1.0",
    "views": [
{"id": "34abd18c-d1c8-489b-af7a-f26786c177d3","hborder": 160,"hport": 480,"hspeed": -1,"hview": 240,"inherit": false,"modelName": "GMRView","objId": "d792344a-0148-48b9-95cf-3879f28ea886","mvc": "1.0","vborder": 120,"visible": true,"vspeed": -1,"wport": 640,"wview": 320,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "e18d1948-2a6f-4b86-987f-d235448f46b0","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "e70c8895-9025-40ce-9246-34b6c0f3e094","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "b831b7c9-27a7-49e9-b82e-5bb72dacd1d3","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "394c8adc-4047-4148-b00e-79d39f8fc679","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "b1f141bc-eb71-4e67-9b7e-c43d5a64664c","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "755f7e5c-b2c1-40ef-ac8d-52e0d5b27f8b","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "7136f083-0bfa-4753-a96c-c2e0ffb286ce","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "38613ae3-f9a0-4a19-985e-ce613ea67d22",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": true,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}