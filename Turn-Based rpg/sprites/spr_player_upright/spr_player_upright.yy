{
    "id": "e53b36fb-5859-451c-8693-c04ac2c2b4a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_upright",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4999053-c949-4098-93db-0d450bc1594e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e53b36fb-5859-451c-8693-c04ac2c2b4a3",
            "compositeImage": {
                "id": "03ad85e4-3fdc-44f7-8ef6-4bef97937b2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4999053-c949-4098-93db-0d450bc1594e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e007a05-046c-4776-b33d-d48752cbbb13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4999053-c949-4098-93db-0d450bc1594e",
                    "LayerId": "8a60a1b2-3c53-4326-99ba-3a782b8bf551"
                }
            ]
        },
        {
            "id": "11170092-5baf-467e-8fa3-aa89ac2089a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e53b36fb-5859-451c-8693-c04ac2c2b4a3",
            "compositeImage": {
                "id": "7210fe0c-2ae4-4433-8065-d615c0b753ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11170092-5baf-467e-8fa3-aa89ac2089a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e952b11-3200-4419-b20a-15986ae2085f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11170092-5baf-467e-8fa3-aa89ac2089a9",
                    "LayerId": "8a60a1b2-3c53-4326-99ba-3a782b8bf551"
                }
            ]
        },
        {
            "id": "0a664e29-2cb8-436e-8728-e17a4f0c1bac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e53b36fb-5859-451c-8693-c04ac2c2b4a3",
            "compositeImage": {
                "id": "d0b3c2a1-5748-48cc-a91f-d7e808d6fedf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a664e29-2cb8-436e-8728-e17a4f0c1bac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0ac4696-0b57-4fd6-8136-46b7b9e0630e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a664e29-2cb8-436e-8728-e17a4f0c1bac",
                    "LayerId": "8a60a1b2-3c53-4326-99ba-3a782b8bf551"
                }
            ]
        },
        {
            "id": "5d4113e0-d18e-4140-9aca-b31c199086fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e53b36fb-5859-451c-8693-c04ac2c2b4a3",
            "compositeImage": {
                "id": "61733d0b-7e2b-4d55-b6a2-5de84df53c02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d4113e0-d18e-4140-9aca-b31c199086fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98d003ec-e109-4518-a4d8-c49d36ae75b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d4113e0-d18e-4140-9aca-b31c199086fa",
                    "LayerId": "8a60a1b2-3c53-4326-99ba-3a782b8bf551"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "8a60a1b2-3c53-4326-99ba-3a782b8bf551",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e53b36fb-5859-451c-8693-c04ac2c2b4a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}