{
    "id": "a2f729f8-be76-419d-81cd-7eec55cb3e76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8723936-cffc-4233-93e0-1eb0aec5945d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2f729f8-be76-419d-81cd-7eec55cb3e76",
            "compositeImage": {
                "id": "644c52eb-86f3-4636-b7ff-387dd376e21b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8723936-cffc-4233-93e0-1eb0aec5945d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db5f9a03-fe63-4e2d-8246-2b786a2454d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8723936-cffc-4233-93e0-1eb0aec5945d",
                    "LayerId": "deedd93d-027c-4c4b-8f53-dc6971efa6c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "deedd93d-027c-4c4b-8f53-dc6971efa6c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2f729f8-be76-419d-81cd-7eec55cb3e76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}