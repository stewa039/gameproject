{
    "id": "1d4e3433-a4d2-4c15-8e76-b5cd3120fe56",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d89f245-60fe-43b8-a311-d057fc62ff63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d4e3433-a4d2-4c15-8e76-b5cd3120fe56",
            "compositeImage": {
                "id": "a346ddf9-2edb-439a-8040-d12cc2726afe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d89f245-60fe-43b8-a311-d057fc62ff63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bc9f2ce-5976-416c-944a-9b918ad2f04b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d89f245-60fe-43b8-a311-d057fc62ff63",
                    "LayerId": "21327897-4f6d-47e3-b583-a16f77039b62"
                }
            ]
        },
        {
            "id": "afe8dd4c-166b-46af-832e-7207fb1fd63c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d4e3433-a4d2-4c15-8e76-b5cd3120fe56",
            "compositeImage": {
                "id": "bc02c797-2b8e-4dab-abc8-100881172f49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afe8dd4c-166b-46af-832e-7207fb1fd63c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b29ce343-5d54-40d8-8e75-36c5dbc8f2ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afe8dd4c-166b-46af-832e-7207fb1fd63c",
                    "LayerId": "21327897-4f6d-47e3-b583-a16f77039b62"
                }
            ]
        },
        {
            "id": "9a55e691-3685-4bc5-a7c1-35dcd29893f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d4e3433-a4d2-4c15-8e76-b5cd3120fe56",
            "compositeImage": {
                "id": "6b0e4c1f-b540-42d8-b71e-f40b8f93104a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a55e691-3685-4bc5-a7c1-35dcd29893f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40bfedea-1c94-49ec-84c9-2c78498b8a13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a55e691-3685-4bc5-a7c1-35dcd29893f7",
                    "LayerId": "21327897-4f6d-47e3-b583-a16f77039b62"
                }
            ]
        },
        {
            "id": "8df8b321-e668-479c-9917-610dad810ae4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d4e3433-a4d2-4c15-8e76-b5cd3120fe56",
            "compositeImage": {
                "id": "830c6043-1aad-44ba-9e80-9dc06f7f344f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8df8b321-e668-479c-9917-610dad810ae4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b215115-ce9f-4a9f-a53a-2c02a5f0f26f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8df8b321-e668-479c-9917-610dad810ae4",
                    "LayerId": "21327897-4f6d-47e3-b583-a16f77039b62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "21327897-4f6d-47e3-b583-a16f77039b62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d4e3433-a4d2-4c15-8e76-b5cd3120fe56",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}