{
    "id": "13e1819e-a284-4900-8559-90b225fdf7ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8aa966ec-666e-40b9-a789-52bd399a9f73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13e1819e-a284-4900-8559-90b225fdf7ac",
            "compositeImage": {
                "id": "cb3a33af-52fe-4da2-907e-6a84d0a85495",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aa966ec-666e-40b9-a789-52bd399a9f73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70969cb7-3db0-4175-82f5-c8e4320dcf2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aa966ec-666e-40b9-a789-52bd399a9f73",
                    "LayerId": "a13d1741-3b17-4fe6-bedb-cf6ad2e2a789"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "a13d1741-3b17-4fe6-bedb-cf6ad2e2a789",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13e1819e-a284-4900-8559-90b225fdf7ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 0
}