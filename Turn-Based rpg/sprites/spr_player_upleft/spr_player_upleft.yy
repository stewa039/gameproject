{
    "id": "b2d0d253-195b-47ff-a0ae-71c22e00d967",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_upleft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "143a7279-cc86-4020-85cc-25c6f3644ebf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2d0d253-195b-47ff-a0ae-71c22e00d967",
            "compositeImage": {
                "id": "e42274ae-b16f-47a8-bf75-fbf7b5d82b19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "143a7279-cc86-4020-85cc-25c6f3644ebf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cf53d5a-3fea-40cc-819b-9a527afbe645",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "143a7279-cc86-4020-85cc-25c6f3644ebf",
                    "LayerId": "8736754c-29d1-4fa2-a53b-dd5a41edc50e"
                }
            ]
        },
        {
            "id": "6cfba7f6-1e71-4bf5-ba89-426b9e0579f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2d0d253-195b-47ff-a0ae-71c22e00d967",
            "compositeImage": {
                "id": "e0c21cec-a130-4deb-9ec6-3f41239ff6f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cfba7f6-1e71-4bf5-ba89-426b9e0579f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f26c705-4fc8-4cf8-8fa6-4497cca0b74b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cfba7f6-1e71-4bf5-ba89-426b9e0579f8",
                    "LayerId": "8736754c-29d1-4fa2-a53b-dd5a41edc50e"
                }
            ]
        },
        {
            "id": "531ea00a-c8a0-4118-8eef-19d581730fb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2d0d253-195b-47ff-a0ae-71c22e00d967",
            "compositeImage": {
                "id": "e1706ff5-2f8c-49ed-bc56-8bb9b85ff072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "531ea00a-c8a0-4118-8eef-19d581730fb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1c87f05-6efc-4fe3-a0c9-b43cce413f9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "531ea00a-c8a0-4118-8eef-19d581730fb7",
                    "LayerId": "8736754c-29d1-4fa2-a53b-dd5a41edc50e"
                }
            ]
        },
        {
            "id": "48a3e55f-018a-44d9-8a71-1adee527fe70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2d0d253-195b-47ff-a0ae-71c22e00d967",
            "compositeImage": {
                "id": "ee4541f7-a743-48f9-bd06-36bd94619dc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48a3e55f-018a-44d9-8a71-1adee527fe70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54e8251f-cc88-417d-88f0-48d3f9cfdcaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48a3e55f-018a-44d9-8a71-1adee527fe70",
                    "LayerId": "8736754c-29d1-4fa2-a53b-dd5a41edc50e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "8736754c-29d1-4fa2-a53b-dd5a41edc50e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2d0d253-195b-47ff-a0ae-71c22e00d967",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}