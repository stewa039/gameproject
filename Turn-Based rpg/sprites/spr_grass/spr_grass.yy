{
    "id": "6ea66521-4298-4d9d-a2e7-85ad93ee5c48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba3970a3-b43e-4a59-b564-82f5375ab108",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ea66521-4298-4d9d-a2e7-85ad93ee5c48",
            "compositeImage": {
                "id": "f9d2788a-3939-4699-998d-b9705edabfa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba3970a3-b43e-4a59-b564-82f5375ab108",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d7860df-30cb-45db-9cc8-d58fd6c190e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba3970a3-b43e-4a59-b564-82f5375ab108",
                    "LayerId": "5c8cfca9-746e-4aeb-904d-45f17bb2e51d"
                }
            ]
        },
        {
            "id": "0d364996-cf06-4093-94fa-1c35c60d33e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ea66521-4298-4d9d-a2e7-85ad93ee5c48",
            "compositeImage": {
                "id": "d942eda6-7f1a-46d8-bf83-cf43669ca16b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d364996-cf06-4093-94fa-1c35c60d33e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe9597b7-6d39-4013-902d-fd723140d455",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d364996-cf06-4093-94fa-1c35c60d33e4",
                    "LayerId": "5c8cfca9-746e-4aeb-904d-45f17bb2e51d"
                }
            ]
        },
        {
            "id": "e34418e0-b237-4902-b439-2e40cd2075bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ea66521-4298-4d9d-a2e7-85ad93ee5c48",
            "compositeImage": {
                "id": "fc260465-0abd-45e2-b3cb-643b3495a75f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e34418e0-b237-4902-b439-2e40cd2075bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15f67d1f-7e80-4775-b3a3-513343869b5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e34418e0-b237-4902-b439-2e40cd2075bd",
                    "LayerId": "5c8cfca9-746e-4aeb-904d-45f17bb2e51d"
                }
            ]
        },
        {
            "id": "df1e43a0-f261-4236-851a-f22c6dc595d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ea66521-4298-4d9d-a2e7-85ad93ee5c48",
            "compositeImage": {
                "id": "877ad178-118b-4bfd-9d01-ae080a2841a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df1e43a0-f261-4236-851a-f22c6dc595d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa2f9665-a4a1-4aa3-b235-6aafc738c3fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df1e43a0-f261-4236-851a-f22c6dc595d8",
                    "LayerId": "5c8cfca9-746e-4aeb-904d-45f17bb2e51d"
                }
            ]
        },
        {
            "id": "538091ca-b966-4835-ba7b-712d3cf0e865",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ea66521-4298-4d9d-a2e7-85ad93ee5c48",
            "compositeImage": {
                "id": "b20c59f7-6bf2-416e-9b6d-476ce3fba860",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "538091ca-b966-4835-ba7b-712d3cf0e865",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a59e2ad-7a56-4a24-8f7f-0b4966f24f52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "538091ca-b966-4835-ba7b-712d3cf0e865",
                    "LayerId": "5c8cfca9-746e-4aeb-904d-45f17bb2e51d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5c8cfca9-746e-4aeb-904d-45f17bb2e51d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ea66521-4298-4d9d-a2e7-85ad93ee5c48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 0
}