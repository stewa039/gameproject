{
    "id": "91ff25fb-e879-49d0-8627-39f7b1126e40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e464669-48f0-4baa-8cc6-ecb5fd35fa0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff25fb-e879-49d0-8627-39f7b1126e40",
            "compositeImage": {
                "id": "6151ed23-ea50-403f-874f-cfa9e0ad313f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e464669-48f0-4baa-8cc6-ecb5fd35fa0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2944f77e-576a-48a4-b4ad-6ef0e616bb76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e464669-48f0-4baa-8cc6-ecb5fd35fa0f",
                    "LayerId": "0a202fcb-f296-4508-99de-e14c9a214c1c"
                }
            ]
        },
        {
            "id": "72a002a8-2c52-404e-988b-ec2c4a868a2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ff25fb-e879-49d0-8627-39f7b1126e40",
            "compositeImage": {
                "id": "26b7e1c9-d19b-49ff-ab32-b398ab2fabc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72a002a8-2c52-404e-988b-ec2c4a868a2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5adcc2c3-0946-4b35-b70e-90b87dae7bdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72a002a8-2c52-404e-988b-ec2c4a868a2c",
                    "LayerId": "0a202fcb-f296-4508-99de-e14c9a214c1c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "0a202fcb-f296-4508-99de-e14c9a214c1c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91ff25fb-e879-49d0-8627-39f7b1126e40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 11
}