{
    "id": "2b20127b-e406-47a4-a89b-2bf1d7e74d42",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_warp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b5217a0-b475-473c-aebd-e7e9ddd2e27d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b20127b-e406-47a4-a89b-2bf1d7e74d42",
            "compositeImage": {
                "id": "a28681d4-dc68-4e88-907f-b2165f0bde55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b5217a0-b475-473c-aebd-e7e9ddd2e27d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "988aff46-3787-4b17-86f0-8a935c91aa76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b5217a0-b475-473c-aebd-e7e9ddd2e27d",
                    "LayerId": "92fb3f4d-396a-42dd-9138-abdefb4b11c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "92fb3f4d-396a-42dd-9138-abdefb4b11c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2b20127b-e406-47a4-a89b-2bf1d7e74d42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}