{
    "id": "51fb3d7f-5ccc-4a1a-b735-469190afd53b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grass_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f83a7db-e089-47fa-b048-94a0955a2bed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51fb3d7f-5ccc-4a1a-b735-469190afd53b",
            "compositeImage": {
                "id": "a442b2fa-05dd-482c-aa29-0f3dbed53966",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f83a7db-e089-47fa-b048-94a0955a2bed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c6152ed-844d-40db-bce0-0a78fa61d3aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f83a7db-e089-47fa-b048-94a0955a2bed",
                    "LayerId": "d72603e9-4e49-4190-ba45-fd192e5e0b08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d72603e9-4e49-4190-ba45-fd192e5e0b08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51fb3d7f-5ccc-4a1a-b735-469190afd53b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}