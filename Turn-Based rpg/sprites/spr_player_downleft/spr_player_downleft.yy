{
    "id": "bb590301-be33-4d1a-a6f0-167471051537",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_downleft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dcb0ab38-5af5-496a-8561-65d378b85c23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb590301-be33-4d1a-a6f0-167471051537",
            "compositeImage": {
                "id": "d99cc9c2-45ec-4032-8bef-31b378439d8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcb0ab38-5af5-496a-8561-65d378b85c23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16e2ec6c-22d1-4040-b6c2-5de4d8d3d355",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcb0ab38-5af5-496a-8561-65d378b85c23",
                    "LayerId": "11dc2adc-aa7b-4b3d-b4ba-7c3760c24484"
                }
            ]
        },
        {
            "id": "6f34b938-1c54-4baa-967c-702befb20865",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb590301-be33-4d1a-a6f0-167471051537",
            "compositeImage": {
                "id": "d403d3c2-13cf-458c-8b3b-d3291bde4916",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f34b938-1c54-4baa-967c-702befb20865",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ef00e4e-c089-4acc-a5b3-4bf801b23d7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f34b938-1c54-4baa-967c-702befb20865",
                    "LayerId": "11dc2adc-aa7b-4b3d-b4ba-7c3760c24484"
                }
            ]
        },
        {
            "id": "c2a0e76a-f618-43aa-9cac-4d002ba90800",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb590301-be33-4d1a-a6f0-167471051537",
            "compositeImage": {
                "id": "e856596f-1c8d-42e9-9062-f014fa27d452",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2a0e76a-f618-43aa-9cac-4d002ba90800",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a778c63-c5af-4e16-bb81-76fcab19cbb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2a0e76a-f618-43aa-9cac-4d002ba90800",
                    "LayerId": "11dc2adc-aa7b-4b3d-b4ba-7c3760c24484"
                }
            ]
        },
        {
            "id": "a897d5d6-943e-4897-a193-d180e93b45ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb590301-be33-4d1a-a6f0-167471051537",
            "compositeImage": {
                "id": "96ff803c-7ba3-45c2-926b-a467321459b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a897d5d6-943e-4897-a193-d180e93b45ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bf45dfc-de9f-400c-a440-d6ff0345d707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a897d5d6-943e-4897-a193-d180e93b45ae",
                    "LayerId": "11dc2adc-aa7b-4b3d-b4ba-7c3760c24484"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "11dc2adc-aa7b-4b3d-b4ba-7c3760c24484",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb590301-be33-4d1a-a6f0-167471051537",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}