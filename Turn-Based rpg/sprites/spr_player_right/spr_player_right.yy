{
    "id": "b4708bca-9317-4404-b564-60b572b97461",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec4a1fe4-65b7-4439-9ac6-acab5b26ff6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4708bca-9317-4404-b564-60b572b97461",
            "compositeImage": {
                "id": "a34b1427-8860-4d33-bb6c-5979a50c0082",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec4a1fe4-65b7-4439-9ac6-acab5b26ff6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05f0475a-b20b-4505-b311-c29dca7914b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec4a1fe4-65b7-4439-9ac6-acab5b26ff6b",
                    "LayerId": "eebc6c58-20ec-4c04-81fb-d4aeff1f76ea"
                }
            ]
        },
        {
            "id": "93273123-83a6-42e8-b49f-26335df8ecef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4708bca-9317-4404-b564-60b572b97461",
            "compositeImage": {
                "id": "cdde98df-64eb-4419-9b73-4e02ac31f0c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93273123-83a6-42e8-b49f-26335df8ecef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08c2b573-971f-4baa-b7ec-8d2df28bb5a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93273123-83a6-42e8-b49f-26335df8ecef",
                    "LayerId": "eebc6c58-20ec-4c04-81fb-d4aeff1f76ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "eebc6c58-20ec-4c04-81fb-d4aeff1f76ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4708bca-9317-4404-b564-60b572b97461",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 11
}