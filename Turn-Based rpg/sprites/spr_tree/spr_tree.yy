{
    "id": "e2f253b4-21b7-4df5-9f16-05a49daf73f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 8,
    "bbox_right": 22,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77dbbe60-29fe-4b40-9c55-604300e7b7b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2f253b4-21b7-4df5-9f16-05a49daf73f9",
            "compositeImage": {
                "id": "58237f9e-e8ee-4f61-b2b3-d7a29fa439db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77dbbe60-29fe-4b40-9c55-604300e7b7b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa8029b9-1779-4bb4-8458-11f79a03c978",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77dbbe60-29fe-4b40-9c55-604300e7b7b5",
                    "LayerId": "f15c82dc-61f1-4199-9e9a-80b10dcd1ecb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f15c82dc-61f1-4199-9e9a-80b10dcd1ecb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2f253b4-21b7-4df5-9f16-05a49daf73f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}