{
    "id": "c4d74799-b0ab-463d-a76f-82574c456c3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_downright",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c91befa6-71a6-4853-918a-b26bacea0593",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d74799-b0ab-463d-a76f-82574c456c3b",
            "compositeImage": {
                "id": "d9023b66-642b-4016-b412-1d01355b5184",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c91befa6-71a6-4853-918a-b26bacea0593",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1344cf1-fc6f-4f74-926b-2a58c62655f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c91befa6-71a6-4853-918a-b26bacea0593",
                    "LayerId": "449136f8-8b48-4f49-a679-03d063d7df51"
                }
            ]
        },
        {
            "id": "aa4ddc72-8776-4860-950e-6fbfc6e7abc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d74799-b0ab-463d-a76f-82574c456c3b",
            "compositeImage": {
                "id": "bbf0f7c5-64cd-4363-9e88-8ebdbfe7b072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa4ddc72-8776-4860-950e-6fbfc6e7abc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc36426d-8e04-4f04-b076-4712e6c54870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa4ddc72-8776-4860-950e-6fbfc6e7abc9",
                    "LayerId": "449136f8-8b48-4f49-a679-03d063d7df51"
                }
            ]
        },
        {
            "id": "7b66a6fe-02ab-49ef-9c02-7a7274944bc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d74799-b0ab-463d-a76f-82574c456c3b",
            "compositeImage": {
                "id": "75766b97-80e5-482a-98e1-8e1ef72f2f1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b66a6fe-02ab-49ef-9c02-7a7274944bc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0ab471e-671c-4359-876b-ea4032ac3d81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b66a6fe-02ab-49ef-9c02-7a7274944bc7",
                    "LayerId": "449136f8-8b48-4f49-a679-03d063d7df51"
                }
            ]
        },
        {
            "id": "d27d81f5-601c-4857-a1a1-f21ebd3a20f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d74799-b0ab-463d-a76f-82574c456c3b",
            "compositeImage": {
                "id": "cf0b532c-3ebc-4c02-bcbd-6550fbfff1da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d27d81f5-601c-4857-a1a1-f21ebd3a20f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce3e7943-a031-495f-9238-6522cb3c1970",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d27d81f5-601c-4857-a1a1-f21ebd3a20f7",
                    "LayerId": "449136f8-8b48-4f49-a679-03d063d7df51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "449136f8-8b48-4f49-a679-03d063d7df51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4d74799-b0ab-463d-a76f-82574c456c3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}