{
    "id": "5e5db9ef-8cf4-4810-b4eb-fe4c9526b433",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d44e6411-d63c-4a26-830a-c7f248891c7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e5db9ef-8cf4-4810-b4eb-fe4c9526b433",
            "compositeImage": {
                "id": "a2bb39cb-0240-42a0-a84d-4fb8dd8ad4b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d44e6411-d63c-4a26-830a-c7f248891c7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f2a2a55-eaba-4411-bceb-21439506dc22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d44e6411-d63c-4a26-830a-c7f248891c7b",
                    "LayerId": "dc98f627-5b1d-46aa-a006-4fc2860dee64"
                }
            ]
        },
        {
            "id": "c929aace-62ca-446a-a481-4b10c5663cc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e5db9ef-8cf4-4810-b4eb-fe4c9526b433",
            "compositeImage": {
                "id": "e5ca2e7e-5c4f-4ecc-aed3-0c551d35dc26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c929aace-62ca-446a-a481-4b10c5663cc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93caf263-0460-4b82-ab27-4c8d9b135fcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c929aace-62ca-446a-a481-4b10c5663cc6",
                    "LayerId": "dc98f627-5b1d-46aa-a006-4fc2860dee64"
                }
            ]
        },
        {
            "id": "dfd41cfc-fb91-4154-afc9-f54d43c291db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e5db9ef-8cf4-4810-b4eb-fe4c9526b433",
            "compositeImage": {
                "id": "0c550ec7-5226-4352-a872-26b6eefa28ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfd41cfc-fb91-4154-afc9-f54d43c291db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e93004f9-d686-4b00-b00e-4a70156717b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfd41cfc-fb91-4154-afc9-f54d43c291db",
                    "LayerId": "dc98f627-5b1d-46aa-a006-4fc2860dee64"
                }
            ]
        },
        {
            "id": "d5c9d101-e503-448d-9a05-480624c156b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e5db9ef-8cf4-4810-b4eb-fe4c9526b433",
            "compositeImage": {
                "id": "ed3ee6db-0640-409f-bfd2-d3fee904ba37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5c9d101-e503-448d-9a05-480624c156b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fe4e083-e5b8-4e6a-9982-767b16752057",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5c9d101-e503-448d-9a05-480624c156b8",
                    "LayerId": "dc98f627-5b1d-46aa-a006-4fc2860dee64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "dc98f627-5b1d-46aa-a006-4fc2860dee64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e5db9ef-8cf4-4810-b4eb-fe4c9526b433",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}