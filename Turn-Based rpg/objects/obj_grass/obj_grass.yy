{
    "id": "8a731cdc-88ed-47b2-8e5c-fb8672ad214a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_grass",
    "eventList": [
        {
            "id": "c3fcde93-04cb-4878-9e56-86b42d87cfe8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8a731cdc-88ed-47b2-8e5c-fb8672ad214a"
        },
        {
            "id": "50d38ed4-bfcc-452a-9409-9d68f3b02a79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8a731cdc-88ed-47b2-8e5c-fb8672ad214a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6ea66521-4298-4d9d-a2e7-85ad93ee5c48",
    "visible": true
}