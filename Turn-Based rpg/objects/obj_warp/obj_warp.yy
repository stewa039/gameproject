{
    "id": "15830417-14e0-42d5-9b73-f826149165d4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_warp",
    "eventList": [
        {
            "id": "6460c742-21a9-4cfb-8df5-39faedc6d0d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "15830417-14e0-42d5-9b73-f826149165d4"
        },
        {
            "id": "2c1395bd-ffd4-4cc7-b2a2-b4394faac19d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "15830417-14e0-42d5-9b73-f826149165d4"
        },
        {
            "id": "35e0a82b-3b25-4950-876b-b3bf269bf36e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d792344a-0148-48b9-95cf-3879f28ea886",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "15830417-14e0-42d5-9b73-f826149165d4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2b20127b-e406-47a4-a89b-2bf1d7e74d42",
    "visible": false
}