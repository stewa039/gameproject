{
    "id": "29d6d5a1-6cc5-4dde-b3b5-399b7cde91c3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tree",
    "eventList": [
        {
            "id": "1375d0d2-d7a1-467c-837b-22ba83687889",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "29d6d5a1-6cc5-4dde-b3b5-399b7cde91c3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ff3ad653-8651-4a32-b130-681b45565207",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e2f253b4-21b7-4df5-9f16-05a49daf73f9",
    "visible": true
}