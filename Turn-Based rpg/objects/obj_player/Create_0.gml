/// @desc intialize
spd = 1;
hspd = 0;
vspd = 0;
len = 0;
dir = 0;

state = move_state;

// sprite variables
face = 0;

#macro RIGHT spr_player_right;
#macro DOWNRIGHT spr_player_downright;
#macro DOWN spr_player_down;
#macro DOWNLEFT spr_player_downleft;
#macro LEFT spr_player_left;
#macro UPLEFT spr_player_upleft;
#macro UPRIGHT spr_player_upright;
#macro UP spr_player_up;
