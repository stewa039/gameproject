{
    "id": "d792344a-0148-48b9-95cf-3879f28ea886",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "3ed927c3-1b90-46d2-bf06-249c85ae8c75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d792344a-0148-48b9-95cf-3879f28ea886"
        },
        {
            "id": "5e5236ee-420b-4680-b5d5-65dfff1d59b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d792344a-0148-48b9-95cf-3879f28ea886"
        },
        {
            "id": "41591873-e0ec-4d0f-b1b7-6a05d7e00ccb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d792344a-0148-48b9-95cf-3879f28ea886"
        }
    ],
    "maskSpriteId": "13e1819e-a284-4900-8559-90b225fdf7ac",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b4708bca-9317-4404-b564-60b572b97461",
    "visible": true
}